# 加载飞桨和其他相关库
import paddle
from paddle.nn import Linear
import paddle.nn.functional as F
import os
import gzip
import json
import random
import numpy as np

# 封装数据读取与处理函数 - 同步数据处理
def load_data(mode='train'):

    # 声明数据集文件位置
    datafile = './work/mnist.json.gz'
    print('loading mnist dataset from {}'.format(datafile))

    # 加载json数据文件
    data = json.load(gzip.open(datafile))
    print('mnist dataset load done')
    # 读取数据分割为训练集、验证集、测试集
    train_set, val_set, eval_set = data

    if mode == 'train':
        # 观察训练集数据
        imgs, labels = train_set[0], train_set[1]
        print('训练集图片数据量：{}'.format(len(imgs)))
        print('训练集标签数据量：{}'.format(len(labels)))
    elif mode == 'valid':
        # 观察验证集数量
        imgs, labels = val_set[0], val_set[1]
        print('验证集图片数据量：{}'.format(len(imgs)))
        print('验证集标签数据量：{}'.format(len(labels)))
    elif mode == 'eval':
        # 观察测试集数据
        imgs, labels = eval_set[0], eval_set[1]
        print('测试集图片数据量：{}'.format(len(imgs)))
        print('测试集标签数据量：{}'.format(len(labels)))
    else:
        raise Exception("mode can only be one of ['train', 'valid', 'eval']")


    print('数据集数量：{}'.format(len(imgs)))
    # 获得数据集长度
    imgs_length = len(imgs)

    # 校验数据
    assert imgs_length == len(labels), \
        "length of train_imgs({}) should be same with train_labels({})".format(
            len(imgs), len(labels)
        )

    # 定义数据集每条数据序号，根据序号读取数据
    index_list = list(range(imgs_length))
    # 训练批次大小
    BATCH_SIZE = 100

    # 随机打乱训练数据的索引
    if mode == 'train':
        random.shuffle(index_list)

    # 定义数据生成器，返回批次数据
    def data_generator():
        imgs_list = []
        labels_list = []
        for i in index_list:
            img = np.array(imgs[i]).astype('float32')
            label = np.array(labels[i]).astype('float32')
            imgs_list.append(img)
            labels_list.append(label)
            if len(imgs_list) == BATCH_SIZE:
                # 获得一个batchsize的数据，并返回
                yield np.array(imgs_list), np.array(labels_list)
                # 清空数据读取列表
                imgs_list = []
                labels_list = []
        # 如果剩余数据的数量小于BATCHSIZE,
        # 则剩余数据一起构成一个大小与len(imgs_list)的mini_batch
        if len(imgs_list) > 0:
            yield np.array(imgs_list), np.array(labels_list)

    return data_generator

    # # 声明数据读取函数，从训练集中读取数据
    # train_loader = data_generator
    # # 以迭代形式读取数据
    # for batch_id, data in enumerate(train_loader()):
    #     img_data, label_data = data
    #     if batch_id == 0:
    #         # 打印数据的shape和类型
    #         print('打印第一个batch数据的维度：')
    #         print('图像维度：{}， 标签维度：{}'.format(
    #             img_data.shape, label_data.shape
    #         ))

# 构建一个类，继承自paddle.io.Dataset,创建数据读取器实现异步数据读取
class RandomDataset(paddle.io.Dataset):
    def __init__(self, num_samples):
        # 样本数量
        self.num_samples = num_samples

    def __getitem__(self, idx):
        # 随机产生数据和label
        image = np.random.random([784]).astype('float32')
        label = np.random.randint(0, 9, (1,)).astype('float32')
        return image, label

    def __len__(self):
        # 返回样本总数量
        return self.num_samples
#
# # 使用DataLoader实现异步数据读取
# dataset = RandomDataset(100)
# loader = paddle.io.DataLoader(dataset, batch_size=3, shuffle=True, drop_last=True, num_workers=2)
#
# for i, data in enumerate(loader()):
#     images, labels = data[0], data[1]
#     print('batch:{}, 训练数据shape：{}, 标签数据shape：{}'.format(
#         i, images.shape, labels.shape
#     ))

class MnistDataset(paddle.io.Dataset):
    def __init__(self, mode):
        datafile = './work/mnist.json.gz'
        data = json.load(gzip.open(datafile))
        # 将数据集分割为训练集，验证集与测试集
        train_set, valid_set, test_set = data[0:3]

        if mode == 'train':
            # 获得训练数据集
            imgs, labels = train_set[0], train_set[1]
        elif mode == 'valid':
            # 获得验证数据集
            imgs, labels = valid_set[0], valid_set[1]
        elif mode == 'test':
            # 获得测试数据集
            imgs, labels = test_set[0], test_set[1]
        else:
            raise Exception("mode can only be one of [train, valid, test]")

        # 校验数据
        imgs_length = len(imgs)
        assert imgs_length == len(labels), \
            "length of train_imgs({}) should be same with train_label({})".format(
                len(imgs), len(labels)
            )

        self.imgs = imgs
        self.labels = labels

    def __getitem__(self, idx):
        img = np.array(self.imgs[idx]).astype('float32')
        label = np.array(self.labels[idx]).astype('float32')

        return img, label

    def __len__(self):
        return len(self.imgs)

# # 声明数据加载实例，使用训练模式，MnistDataset构建迭代器每次迭代只返回batch=1数据
# train_dataset = MnistDataset(mode='train')
# # 使用paddle.io.DataLoader定义DataLoader对象用于加载Python生成器产生的数据
# # DataLoader返回一个批次数据迭代器，并且是异步的
# train_loader = paddle.io.DataLoader(train_dataset, batch_size=100, shuffle=True, num_workers=0)
# # 迭代读取数据并打印数据形状
# for i, data in enumerate(train_loader()):
#     images, labels = data[0:2]
#     print("epoch:{}, 图片shape:{}, 标签shape:{}".format(i, images.shape, labels.shape))
#     if i >=2:
#         break

# 定义网络结构，同baseline.py
class MNIST(paddle.nn.Layer):

     def __init__(self):
         super(MNIST, self).__init__()

         #定义一层全连接层，输出维度为1
         self.fc = paddle.nn.Linear(in_features=784, out_features=1)

     def forward(self, inputs):
         outputs = self.fc(inputs)
         return outputs

# 训练配置，启动训练过程
def train(model):
    # model = MNIST()
    model.train()
    # 调用加载数据函数
    # train_loader = load_data('train')

    # 声明数据加载实例，使用训练模式，MnistDataset构建迭代器每次迭代只返回batch=1数据
    train_dataset = MnistDataset(mode='train')
    # 使用paddle.io.DataLoader定义DataLoader对象用于加载Python生成器产生的数据
    # DataLoader返回一个批次数据迭代器，并且是异步的
    train_loader = paddle.io.DataLoader(train_dataset, batch_size=100, shuffle=True, num_workers=0)

    opt = paddle.optimizer.SGD(learning_rate=0.001, parameters=model.parameters())
    EPOCH_NUM = 10
    mse_loss = paddle.nn.loss.MSELoss()

    for epoch_id in range(EPOCH_NUM):
        for batch_id, data in enumerate(train_loader()):
            images, labels = data[0:2]
            images = paddle.to_tensor(images)
            labels = paddle.to_tensor(labels)

            #前向计算过程
            predicts = model(images)

            #损失计算过程
            loss = mse_loss(predicts, labels)

            if batch_id % 200 == 0:
                print("epoch:{}, batch:{}, loss: {}".format(
                    epoch_id, batch_id, loss.numpy()[0]
                ))

            # 反向传播计算过程
            loss.backward()

            # 网络参数更新过程
            opt.step()

            # 清空梯度
            opt.clear_grad()

    # 保存模型
    paddle.save(model.state_dict(), './mnist.pdparams')


# 创建模型实例
model = MNIST()
# 启动训练过程
train(model)
