# 导入飞桨及其其它相关库
import paddle
from paddle.nn import Conv2D, MaxPool2D, Linear
import paddle.nn.functional as F
import numpy as np
import os
import gzip
import json
import random
from PIL import Image
import matplotlib.pyplot as plt

# 定义数据集,与paddle.io.DataLoader配合使用用于异步数据读取
class MnistDataset(paddle.io.Dataset):
    def __init__(self, data, mode='train'):
        super(MnistDataset, self).__init__()

        # 读取数据集中的训练集，验证集和测试集
        train_set, valid_set, test_set = data[:3]

        # 图片高度和宽度
        self.IMG_ROWS = 28
        self.IMG_COLS = 28

        # 根据输入mode参数决定使用训练集、验证集和测试集
        if mode == 'train':
            imgs, labels = train_set[:2]
        elif mode == 'eval':
            imgs, labels = valid_set[:2]
        elif mode == 'test':
            imgs, labels = test_set[:2]
        else:
            raise Exception('mode must be only one of [train, eval, test]')

        # 获得图片数量
        imgs_length = len(imgs)
        # 验证图片数量和标签数量是否一致
        assert len(imgs) == len(labels), \
            "length of train_imgs({}) should be same as train_labels({})".format(
                len(imgs), len(labels)
            )

        self.imgs = imgs
        self.labels = labels
        self.BATCH_SIZE = 100

    def __getitem__(self, idx):
        img = np.reshape(self.imgs[idx], [1, self.IMG_ROWS, self.IMG_COLS]).astype('float32')
        label = np.reshape(self.labels[idx], [1]).astype('int64')
        return img, label

    def __len__(self):
        return len(self.imgs)

# 定义多层卷积神经网络
class MNIST_CNN(paddle.nn.Layer):
    def __init__(self):
        super(MNIST_CNN, self).__init__()

        self.conv1 = Conv2D(in_channels=1, out_channels=20, kernel_size=5, stride=1, padding=2)
        self.max_pool1 = MaxPool2D(kernel_size=2, stride=2)

        self.conv2 = Conv2D(in_channels=20, out_channels=20, kernel_size=5, stride=1, padding=2)
        self.max_pool2 = MaxPool2D(kernel_size=2, stride=2)

        # in_features = 7x7x20, out_features = class num
        self.fc = Linear(in_features=980, out_features=10)

    def forward(self, inputs, label=None, check_shape=False, check_content=False):
        outputs1 = self.conv1(inputs)
        outputs2 = F.relu(outputs1)
        outputs3 = self.max_pool1(outputs2)

        outputs4 = self.conv2(outputs3)
        outputs5 = F.relu(outputs4)
        outputs6 = self.max_pool2(outputs5)

        outputs6 = paddle.reshape(outputs6, [outputs6.shape[0], 980])
        y = self.fc(outputs6)

        # 选择是否打印神经网络每层的参数尺寸和输出尺寸，验证网络结构是否设置正确
        if check_shape:
            print("\n###################print network layer's superparams################\n")
            print("conv1-- kernel size:{}, padding:{}, stride:{}".format(
                self.conv1.weight.shape, self.conv1._padding, self.conv1._stride
            ))
            print("conv2-- kernel size:{}, padding:{}, stride:{}".format(
                self.conv2.weight.shape, self.conv2._padding, self.conv2._stride
            ))
            print("fc-- weight size:{}, bias_size:{}".format(
                self.fc.weight.shape, self.fc.bias.shape
            ))

            #打印每层的输出尺寸
            print("\n#####################print shape of features of every layer###############\n")
            print("inputs shape:{}".format(inputs.shape))
            print("outputs1_shape:{}".format(outputs1.shape))
            print("outputs2_shape:{}".format(outputs2.shape))
            print("outputs3_shape:{}".format(outputs3.shape))
            print("outputs4_shape:{}".format(outputs4.shape))
            print("outputs5_shape:{}".format(outputs5.shape))
            print("outputs6_shape:{}".format(outputs6.shape))
            print("y_shape:{}".format(y.shape))

        # 选择是否打印训练过程中参数和输出内容，可用于训练过程中调试
        if check_content:
            print("\n############### print convolution layer's kernel#############")
            print("conv1 params -- kernel weights:", self.conv1.weight[0][0])
            print("conv2 params -- kernel weights:", self.conv2.weight[0][0])

            # 创建随机数，随机打印某一通道输出值
            idx1 = np.random.randint(0, outputs1.shape[1])
            idx2 = np.random.randint(0, outputs4.shape[1])
            # 打印卷积-池化后的结果，仅打印batch中第一个图像对应的特征
            print("\nThe {}th channel of conv1 layer: ".format(idx1), outputs1[0][idx1])
            print("\nThe {}th channel of conv2 layer: ".format(idx2), outputs4[0][idx2])
            print("The output of last layer:", y[0], "\n")

        if label is not None:
            acc = paddle.metric.accuracy(input=y, label=label)
            return y, acc
        else:
            return y

# 定义训练过程
def train(model, train_loader):
    # model = MNIST_CNN()
    model.train()

    # 优化算法, 以下四种可逐一尝试
    # opt = paddle.optimizer.SGD(learning_rate=0.001, parameters=model.parameters())
    # opt = paddle.optimizer.Momentum(learning_rate=0.001, momentum=0.9, parameters=model.parameters())
    # opt = paddle.optimizer.Adagrad(learning_rate=0.001, parameters=model.parameters())
    # 优化算法加入正则化项，避免过拟合，参数regularization_coeff调节正则化项的权重
    opt = paddle.optimizer.Adam(learning_rate=0.001, beta1=0.9, beta2=0.999, weight_decay=paddle.regularizer.L2Decay(coeff=1e-5), parameters=model.parameters())

    # 损失函数
    cross_entroy_loss = paddle.nn.loss.CrossEntropyLoss()

    # 迭代次数
    EPOCH_NUM = 10

    # 可视化数据
    iter = 0
    iters = []
    losses = []

    for epoch_id in range(EPOCH_NUM):
        for batch_id, data in enumerate(train_loader()):
            # 准备数据
            images, labels = data[:2]
            images = paddle.to_tensor(images)
            labels = paddle.to_tensor(labels)

            # 前向计算过程
            if batch_id == 0 and epoch_id == 0:
                preds, acc = model(images, labels, check_shape=True, check_content=False)
            elif batch_id == 401:
                preds, acc = model(images, labels, check_shape=False, check_content=True)
            else:
                preds, acc = model(images, labels)

            # 损失计算过程
            loss = cross_entroy_loss(preds, labels)

            if batch_id % 100 == 0:
                print("epoch:{}, batch:{}, loss:{}".format(
                    epoch_id, batch_id, loss.numpy()[0]
                ))

                iters.append(batch_id + epoch_id*len(train_loader()))
                losses.append(loss.numpy()[0])

            # 反向传播计算过程 - 计算每层梯度值
            loss.backward()

            # 网络模型参数更新过程
            opt.step()

            # 清空每层梯度值
            opt.clear_grad()

    # 保存模型
    paddle.save(model.state_dict(), './mnist.pdparams')

    # 绘制训练过程Loss变化曲线
    plt.figure()
    plt.title('train loss', fontsize=24)
    plt.xlabel('iter', fontsize=14)
    plt.ylabel('loss', fontsize=14)
    plt.plot(iters, losses, color='red', label='train loss')
    plt.grid()
    plt.show()

# 加入校验或测试，更好评价模型
def evaluation(model, eval_loader):
    print('start evalution......')
    # 定义预测过程
    params_file_path = './mnist.pdparams'
    # 加载模型参数
    param_dict = paddle.load(params_file_path)
    model.load_dict(param_dict)

    model.eval()

    acc_set = []
    loss_set = []
    cross_entroy_loss = paddle.nn.loss.CrossEntropyLoss()

    for batch_id, data in enumerate(eval_loader()):
        images, labels = data[:2]
        images = paddle.to_tensor(images)
        labels = paddle.to_tensor(labels)

        preds, acc = model(images, labels)
        loss = cross_entroy_loss(preds, labels)
        acc_set.append(float(acc.numpy()))
        loss_set.append(float(loss.numpy()))

    acc_val_mean = np.array(acc_set).mean()
    loss_val_mean = np.array(loss_set).mean()
    print('loss:{}, acc:{}'.format(loss_val_mean, acc_val_mean))

if __name__ == "__main__":

    # GPU设备设置
    use_gpu = True
    paddle.set_device('gpu:0') if use_gpu else paddle.set_device('cpu')

    # 读取数据文件
    datafile = './work/mnist.json.gz'
    print('loading mnist dataset from {}......'.format(datafile))

    data = json.load(gzip.open(datafile))

    # 创建mnist数据集实例
    train_dataset = MnistDataset(data, mode='train')
    train_loader = paddle.io.DataLoader(dataset=train_dataset,
                                        batch_size=100,
                                        shuffle=True,
                                        drop_last=True)

    eval_dataset = MnistDataset(data, mode='eval')
    eval_loader = paddle.io.DataLoader(dataset = eval_dataset,
                                       batch_size=1,
                                       shuffle=False,
                                       drop_last=False)

    # 定义网络模型实例
    model = MNIST_CNN()

    # 启动训练过程
    train(model, train_loader)

    # 启动验证过程
    model1 = MNIST_CNN()
    evaluation(model1, eval_loader)
