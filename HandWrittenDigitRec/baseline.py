# 加载飞桨和相关库
import paddle
from paddle.nn import Linear
import paddle.nn.functional as F
import os
import numpy as np
import matplotlib.pyplot as plt

# 1. 数据处理
# 设置数据读取器， API自动读取mnist数据训练集
# train_dataset = paddle.vision.datasets.MNIST(mode='train')

#
# # 读取任意一条数据内容并打印结果
# train_data0 = np.array(train_dataset[0][0])
# train_label0 = np.array(train_dataset[0][1])
#
# # 显示第一batch的第一个图像
# plt.figure("Image") # 窗口名称
# plt.figure(figsize=(2,2))
# plt.imshow(train_data0, cmap=plt.cm.binary)
# plt.axis('on') # 打开坐标抽
# plt.title('image') # 图像标题
# plt.show()

# print("图像数据形状：{}".format(train_data0.shape))
# print("图像标签形状：{}，图像标签值：{}".format(train_label0.shape, train_label0))

# 定义mnist数据识别网络结构，同房价预测网络
class MNIST(paddle.nn.Layer):
    def __init__(self):
        super(MNIST, self).__init__()

        # 定义一层全连接层，输出维度为1
        self.fc = paddle.nn.Linear(in_features=784, out_features=1)

    # 定义网络结构的前向计算过程
    def forward(self, inputs):
        outputs = self.fc(inputs)
        return outputs

# 图像归一化函数，将图像数据范围从【0-255】的图像归一化到【0,1】
def norm_img(img):
    # 验证输入数据格式是否正确，img的shape为【batch_size, 28, 28]
    assert len(img.shape) == 3
    batch_size, img_h, img_w = img.shape[0:3]
    # 归一化图像数据
    img = img / 255
    #将图像形式reshape为【batch_size, 784]
    img = paddle.reshape(img, [batch_size, img_h*img_w])

    return img

#确保从paddle.vision.datasets.MNIST中加载的图像数据是np.ndarray类型
paddle.vision.set_image_backend('cv2')

# 定义训练过程
def train(model):
    # 启动训练状态
    model.train()
    # 加载训练集 batch_size=16
    train_loader = paddle.io.DataLoader(paddle.vision.datasets.MNIST(mode='train'),
                                        batch_size=16,
                                        shuffle=True)
    #定义优化器，使用随机梯度下降SGD优化器，学习率设置为0.001
    opt = paddle.optimizer.SGD(learning_rate=0.001, parameters=model.parameters())
    EPOCH_NUM = 10

    # 定义损失函数
    mse_loss = paddle.nn.MSELoss()

    for epoch in range(EPOCH_NUM):
        for batch_id, data in enumerate(train_loader()):
            images = norm_img(data[0]).astype('float32')
            labels = data[1].astype('float32')

            # 前向计算
            predicts = model(images)

            # 损失计算
            loss = mse_loss(predicts, labels)
            if batch_id % 1000 == 0:
                print('epoch:{}, batch:{}, loss:{}'.format(
                    epoch, batch_id, loss
                ))
            # 反向传播 - 梯度计算
            loss.backward()
            # 参数更新
            opt.step()
            # 清空梯度
            opt.clear_grad()

# 声明网络结构
model = MNIST()
train(model)
paddle.save(model.state_dict(), './mnist.pdparams')

# 推理过程
# 读取一张本地图片，转变为模型输入格式
def load_image(img_path):
    from PIL import Image
    # 从img_path中读取图像，并转换为灰度图
    im = Image.open(img_path).convert('L')
    im = im.resize((28, 28), Image.ANTIALIAS)
    im = np.array(im).reshape(1, -1).astype(np.float32)
    # 图像归一化
    im = im / 255
    return im

# 定义预测过程
model = MNIST()
params_file_path = './mnist.pdparams'
img_path = './images/0.jpg'
# 加载模型参数
param_dict = paddle.load(params_file_path)
model.load_dict(param_dict)
# 载入测试数据
tensor_img = load_image(img_path)
model.eval()
result = model(paddle.to_tensor(tensor_img))
print('resut:{}'.format(result))
# 预测输出取整
print('本次预测数据是：{}'.format(result.numpy().astype('int32')))