import numpy as np
import DataProcess as dp
import matplotlib.pyplot as plt

class Network(object):
    def __init__(self, num_of_weights):
        # fix random seek in order to keep repeatabilityl
        np.random.seed(0)
        self.w = np.random.randn(num_of_weights, 1)
        self.b = 0

    def forward(self, x):
        z = np.dot(x, self.w) + self.b
        return z

    def loss(self, z, y):
        error = z - y
        cost = error * error
        cost = np.sum(cost) / error.shape[0]
        return cost

    def gradient(self, x, y):
        z = self.forward(x)
        gradient_w = (z - y) * x
        gradient_w = np.mean(gradient_w, axis=0)
        gradient_w = gradient_w[:, np.newaxis]
        gradient_b = (z - y)
        gradient_b = np.mean(gradient_b)
        return gradient_w, gradient_b

    def update(self, gradient_w, gradient_b, eta = 0.01):
        self.w = self.w - eta * gradient_w
        self.b = self.b - eta * gradient_b

    def train_1(self, x, y, iterations = 100, eta = 0.01):
        # points = []
        losses = []
        for i in range(iterations):
            # points.append([net.w[5][0], net.w[9][0]])
            z = self.forward(x)
            L = self.loss(z, y)
            gradient_w, gradient_b = self.gradient(x, y)
            #gradient_w5 = gradient_w[5][0]
            #gradient_w9 = gradient_w[9][0]
            #self.update(gradient_w5, gradient_w9, eta)
            self.update(gradient_w, gradient_b, eta)
            losses.append(L)
            if (i+1) % 10 == 0:
                # print('iter {}, point {}, loss {}'.format(i, [net.w[5][0], net.w[9][0]], L))
                print('iter {}, loss {}'.format(i, L))
        # return points, losses
        return losses

    def train(self, training_data, num_epochs, batch_size=10, eta=0.01):
        n = len(training_data)
        losses = []
        for epoch_id in range(num_epochs):
            # 在每轮迭代开始之前，将训练数据的顺序随机打乱
            # 然后再按每次取batch_size条数据的方式取出
            np.random.shuffle(training_data)
            # 将训练数据拆分，每个mini_batch包含batch_size条的数据
            mini_batches = [training_data[k: k+batch_size] for k in range(0, n, batch_size)]
            for iter_id, mini_batch in enumerate(mini_batches):
                x = mini_batch[:, :-1]
                y = mini_batch[:, -1:]
                y_pred = self.forward(x)
                loss = self.loss(y_pred, y)
                gradient_w, gradient_b = self.gradient(x, y)
                self.update(gradient_w, gradient_b, eta)
                losses.append(loss)
                print('Epoch: {:3d} / iter {:3d}, loss = {:.4f}'.format(
                    epoch_id, iter_id, loss
                ))

        return losses

# 获取数据
train_data, test_data = dp.load_data()
x = train_data[:, :-1]
y = train_data[:, -1:]

# 创建网络
net = Network(13)
num_iterations = 2000

# 启动训练
# points, losses = net.train(x, y, iterations=num_iterations, eta=0.01)
# losses = net.train(x, y, iterations=num_iterations, eta = 0.01)

losses = net.train(train_data, num_epochs=50, batch_size=100, eta=0.01)

# 绘制损失函数变化趋势
# plot_x = np.arange(num_iterations)
plot_x = np.arange(len(losses))
plot_y = np.array(losses)
plt.plot(plot_x, plot_y)
plt.show()

#
# training_data, test_data = dp.load_data()
# x = training_data[:, :-1]
# y = training_data[:, -1:]
#
#
# net = Network(13)
# x1 = x[0:3]
# y1 = y[0:3]
# z = net.forward(x1)
# print('predict:', z)
# loss = net.loss(z, y1)
# print('loss:', loss)
#
#
# net = Network(13)
# losses = []
# # only draw parameter w5 and w9 curve line which belong to the range[-160, 160]
# w5 = np.arange(-160.0, 160.0, 1.0)
# w9 = np.arange(-160.0, 160.0, 1.0)
# losses = np.zeros([len(w5), len(w9)])
#
# # calculate every loss for every w5 and w9
# for i in range(len(w5)):
#     for j in range(len(w9)):
#         net.w[5] = w5[i]
#         net.w[9] = w9[j]
#         z = net.forward(x)
#         loss = net.loss(z, y)
#         losses[i, j] = loss
#
# # draw 3D graph
# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
# fig = plt.figure()
# ax = Axes3D(fig)
#
# w5, w9 = np.meshgrid(w5, w9)
# ax.plot_surface(w5, w9, losses, rstride=1, cstride=1, cmap='rainbow')
# plt.show()
#
# # calculate gradient
# z = net.forward(x)
# gradient_w = (z - y) * x
# gradient_w = np.mean(gradient_w, axis=0)
# gradient_w = gradient_w[:, np.newaxis]
#
# gradient_b = (z - y)
# gradient_b = np.mean(gradient_b)

# # 获取数据
# train_data, test_data = dp.load_data()
#
# # 打乱样本顺序
# np.random.shuffle(train_data)
#
# # 将train_data分成多个mini_batch
# batch_size = 10
# n = len(train_data)
# mini_batches = [train_data[k: k+batch_size] for k in range(0, n, batch_size)]
#
# # 创建网络
# net = Network(13)
#
# # 依次使用每个mini_batch的数据
# for mini_batch in mini_batches:
#     x = mini_batch[:, :-1]
#     y = mini_batch[:, -1:]
#     loss = net.train(x, y, iterations=1)
