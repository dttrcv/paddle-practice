#加载飞桨、Numpy和相关类库

#飞桨主库
import paddle
# 组网相关API，比如Linear, Conv2D, LSTM, CrossEntropyLoss, ReLU等
# Linear:神经网络的全连接层函数
from paddle.nn import Linear
# 与paddle.nn一样，包含组网相关API,两者功能相同，运行性能也基本一致，但是paddle.nn下均是类， functional下均是函数
import paddle.nn.functional as F

import numpy as np
import os
import random

# 1. 数据处理
def load_data():
    # 从文件导入数据
    datafile = './dataset/housing.data'
    data = np.fromfile(datafile, sep=' ', dtype = np.float32)

    # 每条数据包含14项，其中前面13项是影响因素 - 特征，第14项是相应的房屋价格中位数 - 真实值Ground Truth
    feature_names = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', \
                      'DIS', 'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV']

    feature_num = len(feature_names)

    # 将原始数据进行reshape，变成【N, 14]这样的形状
    data = data.reshape([data.shape[0] // feature_num, feature_num])

    # 将原始数据集拆分成训练集80%和测试集20%
    # 训练集和测试集是互斥的
    ratio = 0.8
    offset = int(data.shape[0] * 0.8)
    train_data = data[:offset]

    # 计算train数据集的最大值、最小值、平均值
    maximums, minimums, avgs = train_data.max(axis=0), train_data.min(axis=0), \
        train_data.sum(axis=0) / train_data.shape[0]

    # 记录数据的归一化参数，在预测时对数据做归一化
    global max_values
    global min_values
    global avg_values
    max_values = maximums
    min_values = minimums
    avg_values = avgs

    # 对数据进行归一化处理
    for i in range(feature_num):
        data[:, i] = (data[:, i] - avgs[i]) / (maximums[i] - minimums[i])

    # 训练集和测试集划分
    train_data = data[:offset]
    test_data = data[offset:]
    return train_data, test_data

# 2. 模型设计
class Regressor(paddle.nn.Layer):

    def __init__(self):
        super(Regressor, self).__init__()

        # 定义一层全连接层，输入维度13, 输出维度1
        self.fc = Linear(in_features=13, out_features=1)

    # 网络的前向计算
    def forward(self, inputs):
        x = self.fc(inputs)
        return x

# 3. 训练配置
# 3.1 声明定义好的线性回归模型
model = Regressor()
# 3.2 开启模型训练模式
model.train()
# 3.3 加载数据
train_data, test_data = load_data()
# 3.4 定义优化算法，使用随机梯度下降SGD, 学习率设置为0.01
opt = paddle.optimizer.SGD(learning_rate=0.01, parameters=model.parameters())

# 4. 训练过程
EPOCH_NUM = 10 # 数据集训练次数
BATCH_SIZE = 10 # 数据集每个Batch大小

for epoch_id in range(EPOCH_NUM):
    # 在每轮迭代开始之前，将训练数据的顺序随机打乱
    np.random.shuffle(train_data)
    # 将训练数据进行拆分，每个batch包含10条数据
    mini_batches = [train_data[k: k+BATCH_SIZE] for k in range(0, len(train_data), BATCH_SIZE)]
    for iter_id, mini_batch in enumerate(mini_batches):
        x = np.array(mini_batch[:, :-1]) # 获得当前批次训练数据
        y = np.array(mini_batch[:, -1:]) #获得当前批次训练标签
        # 将numpy转为飞桨动态图tensor类型
        house_features = paddle.to_tensor(x)
        prices = paddle.to_tensor(y)

        # 前向计算
        predicts = model(house_features)

        # 计算损失
        loss = paddle.nn.loss.MSELoss()
        avg_loss = loss(predicts, prices)

        if iter_id % 20 == 0:
            print('epoch:{}, iter:{}, loss is: {}'.format(
                epoch_id, iter_id, avg_loss.numpy()[0]
            ))

        # 反向计算
        avg_loss.backward()

        # 最小化loss, 更新参数
        opt.step()

        # 清空梯度
        opt.clear_grad()

# 5. 保存并测试模型
# 5.1 保存模型
# 保存模型参数，文件名为LR_model.pdparams
paddle.save(model.state_dict(), 'LR_model.pdparams')
print('模型保存成功， 模型参数保存在LR_model.pdparams中')

# 5.2 测试模型

def load_one_example():
    # 从上边已加载的测试集中，随机选择一个作为测试数据
    idx = np.random.randint(0, test_data.shape[0])
    idx = -10
    one_data, label = test_data[idx, :-1], test_data[idx, -1:]
    # 修改该条数据shape为[1, 13]
    one_data = one_data.reshape([1, -1])

    return one_data, label

# 加载模型参数
model_dict = paddle.load('LR_model.pdparams')
model.load_dict(model_dict)
# 切换模型状态：预测状态
model.eval()

# 加载1条测试数据
one_data, label = load_one_example()
# 将数据转为动态图的variable格式
one_data = paddle.to_tensor(one_data)
predict = model(one_data)

# 对结果进行反归一化处理
predict = predict * (max_values[-1] - min_values[-1]) + avg_values[-1]
# 对label数据做反归一化处理
label = label * (max_values[-1] - min_values[-1]) + avg_values[-1]

print('Inference result is {}, the corresponding label is {}.'.format(
    predict.numpy(), label
))