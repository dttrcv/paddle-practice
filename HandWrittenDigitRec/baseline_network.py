# 导入飞桨和相关库
import paddle
import paddle.nn.functional as F
from paddle.nn import Linear, Conv2D, MaxPool2D
import numpy as np
import os
import gzip
import matplotlib.pyplot as plt
import random
import json


# 激活函数
def sigmoid(x):
    return 1.0 / (1 + np.exp(-x))

# x = np.arange(-8, 8, 0.2)
# y = sigmoid(x)
# plt.plot(x, y)
# plt.show()

# 封装数据读取与处理函数 - 同步数据处理
def load_data(mode='train'):

    # 声明数据集文件位置
    datafile = './work/mnist.json.gz'
    print('loading mnist dataset from {}'.format(datafile))

    # 加载json数据文件
    data = json.load(gzip.open(datafile))
    print('mnist dataset load done')
    # 读取数据分割为训练集、验证集、测试集
    train_set, val_set, eval_set = data

    if mode == 'train':
        # 观察训练集数据
        imgs, labels = train_set[0], train_set[1]
        print('训练集图片数据量：{}'.format(len(imgs)))
        print('训练集标签数据量：{}'.format(len(labels)))
    elif mode == 'valid':
        # 观察验证集数量
        imgs, labels = val_set[0], val_set[1]
        print('验证集图片数据量：{}'.format(len(imgs)))
        print('验证集标签数据量：{}'.format(len(labels)))
    elif mode == 'eval':
        # 观察测试集数据
        imgs, labels = eval_set[0], eval_set[1]
        print('测试集图片数据量：{}'.format(len(imgs)))
        print('测试集标签数据量：{}'.format(len(labels)))
    else:
        raise Exception("mode can only be one of ['train', 'valid', 'eval']")


    print('数据集数量：{}'.format(len(imgs)))
    # 获得数据集长度
    imgs_length = len(imgs)

    # 校验数据
    assert imgs_length == len(labels), \
        "length of train_imgs({}) should be same with train_labels({})".format(
            len(imgs), len(labels)
        )

    # 定义数据集每条数据序号，根据序号读取数据
    index_list = list(range(imgs_length))
    # 训练批次大小
    BATCH_SIZE = 100

    # 随机打乱训练数据的索引
    if mode == 'train':
        random.shuffle(index_list)

    IMG_ROWS, IMG_COLS = 28, 28

    # 定义数据生成器，返回批次数据
    def data_generator():
        imgs_list = []
        labels_list = []
        for i in index_list:
            img = np.array(imgs[i]).astype('float32')
            label = np.array(labels[i]).astype('float32')
            # 使用卷积神经网络结构时，uncomment下面两行代码
            img = np.reshape(img, [1, IMG_ROWS, IMG_COLS]).astype('float32')
            label = np.reshape(label, [1]).astype('float32')

            imgs_list.append(img)
            labels_list.append(label)
            if len(imgs_list) == BATCH_SIZE:
                # 获得一个batchsize的数据，并返回
                yield np.array(imgs_list), np.array(labels_list)
                # 清空数据读取列表
                imgs_list = []
                labels_list = []
        # 如果剩余数据的数量小于BATCHSIZE,
        # 则剩余数据一起构成一个大小与len(imgs_list)的mini_batch
        if len(imgs_list) > 0:
            yield np.array(imgs_list), np.array(labels_list)

    return data_generator

# 定义多层全连接神经网络
class MNIST_MLP(paddle.nn.Layer):
    def __init__(self):
        super(MNIST_MLP, self).__init__()

        # 定义两层全连接隐含层，输出维度为10
        self.fc1 = Linear(in_features=784, out_features=10)
        self.fc2 = Linear(in_features=10, out_features=10)
        # 定义一层全连接输出层，输出维度为1
        self.fc3 = Linear(in_features=10, out_features=1)

    # 前向计算函数
    def forward(self, inputs):
        outputs1 = self.fc1(inputs)
        outputs1 = F.sigmoid(outputs1)
        outputs2 = self.fc2(outputs1)
        outputs2 = F.sigmoid(outputs2)
        outputs_final = self.fc3(outputs2)
        return outputs_final

# 定义多层卷积神经网络
class MNIST_CNN(paddle.nn.Layer):
    def __init__(self):
        super(MNIST_CNN, self).__init__()

        # 定义卷积层，输出通道数为20，卷积核大小为5， 卷积步长为1，填充为2
        self.conv1 = Conv2D(in_channels=1, out_channels=20, kernel_size=5, stride=1, padding=2)
        # 定义池化层，池化核大小为2， 步长为2
        self.max_pool1 = MaxPool2D(kernel_size=2, stride=2)
        # 定义卷积层，输出通道为20， 卷积核大小为5， 卷积步长为1， 填充为2
        self.conv2 = Conv2D(in_channels=20, out_channels=20, kernel_size=5, stride=1, padding=2)
        # 定义池化层，池化核大小为2， 池化步长为2
        self.max_pool2 = MaxPool2D(kernel_size=2, stride=2)
        # 定义一层全连接层，输出维度为1
        self.fc = Linear(in_features=980, out_features=1)

    # 定义网络前向计算过程
    def forward(self, inputs):
        x = self.conv1(inputs)
        x = F.relu(x)
        x = self.max_pool1(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = self.max_pool2(x)
        x = paddle.reshape(x, [x.shape[0], -1])
        x = self.fc(x)
        return x

# 定义训练过程
def train(model):
    # model = MNIST_CNN()
    model.train()
    # 调用加载数据的函数，获得mnist数据集
    train_loader = load_data(mode='train')
    # 使用SGD优化器,合适的学习率很重要，0.01， 0.0001，和0.001相比损失值下降的慢
    opt = paddle.optimizer.SGD(learning_rate=0.001, parameters=model.parameters())
    # MSE损失函数
    mse_loss = paddle.nn.loss.MSELoss()

    # 训练轮次
    EPOCH_NUM = 10
    # MNIST图像高和宽
    IMG_ROWS, IMG_COLS = 28, 28

    for epoch_id in range(EPOCH_NUM):
        for batch_id, data in enumerate(train_loader()):
            # 准备数据
            images, labels = data[:2]
            images = paddle.to_tensor(images)
            labels = paddle.to_tensor(labels)
            # 使用卷积神经网络时，uncomment 下面这行代码
            images = paddle.reshape(images, [images.shape[0], 1, IMG_ROWS, IMG_COLS])

            # 前向计算过程
            predicts = model(images)

            # 损失计算过程
            loss = mse_loss(predicts, labels)

            if batch_id % 200 == 0:
                print('epoch:{}, batch:{}, loss:{}'.format(
                    epoch_id, batch_id, loss.numpy()[0]
                ))

            # 反向传播计算过程
            loss.backward()

            # 网络模型参数更新过程
            opt.step()

            # 清空梯度
            opt.clear_grad()

    # 保存模型参数
    paddle.save(model.state_dict(), './mnist.pdparams')

# 创建模型实例
model = MNIST_CNN()
# 启动训练过程
train(model)