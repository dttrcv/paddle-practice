import numpy as np
import json

def load_data():
    # 1. read data
    datafile = './dataset/housing.data'
    data = np.fromfile(datafile, sep=' ')
    print(data)

    # 2. transform data shape
    feature_names = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE','DIS',
                     'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV' ]
    feature_num = len(feature_names)
    data = data.reshape([data.shape[0] // feature_num, feature_num])

    x = data[0]
    print(x.shape)
    print(x)

    # 3. dataset split: 80% training set and 20% testing set
    ratio = 0.8
    offset = int(data.shape[0] * ratio)
    training_data = data[:offset]
    print(training_data.shape)

    # 4. data normalization
    maximums, minimums, avgs = training_data.max(axis=0), \
        training_data.min(axis=0),\
        training_data.sum(axis=0) / training_data.shape[0]

    for i in range(feature_num):
        data[:, i] = (data[:, i] - minimums[i]) / (maximums[i] - minimums[i])

    training_data = data[:offset]
    test_data = data[offset:]
    return training_data, test_data

# get data
training_data, test_data = load_data()
x = training_data[:, :-1]
y = training_data[:, -1:]
print(x[0])
print(y[0])